package Package01;

/**
 * @author Rona Dumitrescu
 * @date 21/03/2019
 *
 */

public class Monom implements Comparable<Monom>{

	private Float coef;
	private Integer grad;

	// CONSTRUCTORI
	public Monom()
	{
		coef =  Float.valueOf(0);
		grad =  Integer.valueOf(0);
	}
	public Monom(Float f, Integer i)
	{
		this.setCoef(f);
		this.setGrad(i);
	}
	
	// SETTERS&GETTES
	public Float getCoef() {
		return coef;
	}
	
	public void setCoef(Float coef) {
		this.coef = coef;
	}
	
	public Integer getGrad() {
		return grad;
	}
	
	public void setGrad(Integer grad) {
		this.grad = grad;
	}
	
	/**
	 *  Metoda de comparare a Obiectelor Monom
	 */
	public int compareTo(Monom m) {
		
		if(getGrad() == null || m.getGrad() == null )
		{
			return 0;
		}
		
		return getGrad().compareTo(m.getGrad());
	}
	
}
