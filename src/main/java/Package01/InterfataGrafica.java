package Package01;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;


public class InterfataGrafica extends JFrame implements ActionListener{
	
	private JPanel panel1;
	private JPanel panel2;
	private JLabel polinom1Label;
	private JLabel polinom2Label;
	private JLabel intro;
	private JLabel rezultatLabel;

	private JTextField polinom1;
	private JTextField polinom2;
	private JTextField rezultat;
	
	private JButton adunare;
	private JButton scadere;
	private JButton inmultire;
	private JButton impartire;
	private JButton derivare;
	private JButton integrare;
	
	
	public InterfataGrafica()
	{
		super("Sistem de procesare a polinoamelor");
		setLayout(new FlowLayout());
		
		//PANELS
		panel1 = new JPanel();
		panel1.setLayout(new  BoxLayout(panel1, BoxLayout.PAGE_AXIS));
		panel2 = new JPanel();
		panel2.setLayout(new  FlowLayout());
		
		//LABELS
		intro = new JLabel("Introduceti polinoamele: ");
		polinom1Label = new JLabel("Polinom 1");
		polinom2Label = new JLabel("Polinom 2");
		rezultatLabel = new JLabel("Rezultat");

		//TEXTFIELDS
		final JTextField polinom1 = new JTextField(30);
		final JTextField polinom2 = new JTextField(30);
		final JTextField rezultat = new JTextField(40);
		
		//BUTTONS

		final JButton adunare = new JButton();
		adunare.setText("+");
		adunare.addActionListener(this);
		
		JButton scadere = new JButton();
		scadere.setText("-");
		
		JButton inmultire = new JButton();
		inmultire.setText("*");
		
		JButton impartire = new JButton();
		impartire.setText("/");
		
		JButton derivare = new JButton();
		derivare.setText("'");
		
		JButton integrare = new JButton();
		integrare.setText("~");
		
		// ADDING PANELS TO FRAME
		add(panel1); 
		add(panel2);
		
		// ADDING COMPONENTS TO PANELS
		panel1.add(intro);
	
		panel1.add(polinom1Label);
		panel1.add(polinom1);
		
		panel1.add(polinom2Label);
		panel1.add(polinom2);
	
		panel1.add (rezultatLabel);
		panel1.add(rezultat);
		
		panel2.add(adunare);
		panel2.add(scadere);
		panel2.add(inmultire);
		panel2.add(impartire);
		panel2.add(derivare);	
		panel2.add(integrare);
						
		// ADDING ACTION LISTENERS
		adunare.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) {
				
			  	Polinom Ppolinom1 = new Polinom();
		    	Polinom Ppolinom2 = new Polinom();
		    	
		    	String input = polinom1.getText();
		    	Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)");  // "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)"   - merge
		    	Matcher m = p.matcher( input );
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului    	
		    	    Ppolinom1.addMonom(monom);  // adaugam monomul in lista	
		    	}	
		    	
		    	String input2 = polinom2.getText();
		    	m = p.matcher(input2);
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului	    	
		    	    Ppolinom2.addMonom(monom);  // adaugam monomul in lista
		    	}	

		    	rezultat.setText(Operatii.adunare(Ppolinom1, Ppolinom2).printInFormatPolinom());
			}

		});
	
		
		scadere.addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e)
					{
						Polinom Ppolinom1 = new Polinom();
				    	Polinom Ppolinom2 = new Polinom();
				    	
				    	String input = polinom1.getText();
				    	Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)");  // "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)"   - merge
				    	Matcher m = p.matcher( input );
				    	
				    	while (m.find())
				    	{
				    		Monom monom = new Monom();
				    		 monom.setCoef(Float.valueOf(m.group(1)));
				    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului    	
				    	    Ppolinom1.addMonom(monom);  // adaugam monomul in lista	
				    	}	
				    	
				    	String input2 = polinom2.getText();
				    	m = p.matcher(input2);
				    	
				    	while (m.find())
				    	{
				    		Monom monom = new Monom();
				    		 monom.setCoef(Float.valueOf(m.group(1)));
				    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului	    	
				    	    Ppolinom2.addMonom(monom);  // adaugam monomul in lista
				    	}	

				    	rezultat.setText(Operatii.scadere(Ppolinom1, Ppolinom2).printInFormatPolinom());
					}
				});
		
		inmultire.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Polinom Ppolinom1 = new Polinom();
		    	Polinom Ppolinom2 = new Polinom();
		    	
		    	String input = polinom1.getText();
		    	Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)");  // "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)"   - merge
		    	Matcher m = p.matcher( input );
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului    	
		    	    Ppolinom1.addMonom(monom);  // adaugam monomul in lista	
		    	}	
		    	
		    	String input2 = polinom2.getText();
		    	m = p.matcher(input2);
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului	    	
		    	    Ppolinom2.addMonom(monom);  // adaugam monomul in lista
		    	}	

		    	rezultat.setText(Operatii.inmultire(Ppolinom1, Ppolinom2).printInFormatPolinom());
			}
		});
		
		impartire.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				
			}
		});
		
		derivare.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Polinom Ppolinom1 = new Polinom();
		    	Polinom Ppolinom2 = new Polinom();
		    	
		    	String input = polinom1.getText();
		    	Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)");  // "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)"   - merge
		    	Matcher m = p.matcher( input );
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului    	
		    	    Ppolinom1.addMonom(monom);  // adaugam monomul in lista	
		    	}	

		    	rezultat.setText(Operatii.derivare(Ppolinom1).printInFormatPolinom());
			}
		});
		
		integrare.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Polinom Ppolinom1 = new Polinom();
		    	Polinom Ppolinom2 = new Polinom();
		    	
		    	String input = polinom1.getText();
		    	Pattern p = Pattern.compile("([+-]?\\d*\\.?\\d*)[xX]\\^(-?\\d+\\b)");  // "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)"   - merge
		    	Matcher m = p.matcher( input );
		    	
		    	while (m.find())
		    	{
		    		Monom monom = new Monom();
		    		 monom.setCoef(Float.valueOf(m.group(1)));
		    		 monom.setGrad(Integer.valueOf(m.group(2)));  // gradul monomului    	
		    	    Ppolinom1.addMonom(monom);  // adaugam monomul in lista	
		    	}	

		    	rezultat.setText(Operatii.integrare(Ppolinom1).printInFormatPolinom());
			}
		});

	
	}


	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}



}
