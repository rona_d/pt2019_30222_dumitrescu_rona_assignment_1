package Package01;

import java.util.ArrayList;
import java.util.List;


public class Polinom {

	List<Monom> listaPolinom = new ArrayList<Monom>();

	public List<Monom> getListaPolinom() {
		return listaPolinom;
	}

	public void setListaPolinom(List<Monom> listaPolinom) {
		this.listaPolinom = listaPolinom;
	}
	
	
	/**
	 *  Metoda primeste un Obiect de tipul Monom pentru a-l adauga in lista de monoame (polinom)
	 *  @param monom - Monomul care trebuie adaugat
	 */
	public void addMonom(Monom monom) {

		if (listaPolinom.isEmpty() == true) // daca lista e vida, adaugam monomul
		{
			listaPolinom.add(monom);
		}

		else { 
			
			boolean b = false;
			for (Monom mSearch : listaPolinom) {

				if (mSearch.getGrad().equals(monom.getGrad())) // daca au acelasi grad, se aduna coeficientii monomului
				{
					mSearch.setCoef(mSearch.getCoef() + monom.getCoef());
					b = true;
					break;
				}
			}
			
			if(!b)
			{
				listaPolinom.add(monom);
			}
		}
	}

	
	/**
	 *  Metoda prinInFormatPolinom printeza polinomul in format matematic
	 *  @return - Polinomul sub forma de string
	 */
	public String printInFormatPolinom()
	{
		
		String sPolinom = new String(" ");
		for (Monom mPrint : listaPolinom) {
		
			if(mPrint.getCoef() >= 0 )
			{
				sPolinom = "+" + mPrint.getCoef().toString() +"x^" +mPrint.getGrad().toString() + sPolinom;
			}
			else
			{
				sPolinom =mPrint.getCoef().toString() +"x^" +mPrint.getGrad().toString() + sPolinom;
			}
				
		
		
		}
		
		return sPolinom;
	}



}
