package Package01;


import javax.swing.*;

/**
 * @author Rona Dumitrescu
 * @date 21/03/2019
 *
 */


public class App 
{
	/**
	 * Prin apelarea acestei metode deschidem interfata/ fereastra aplicatiei
	 */
	 private static void  interfGraf()
	    {	 
		 
	        InterfataGrafica interfG = new InterfataGrafica();
	        interfG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        interfG.setSize(500,260);
	        interfG.setLocationRelativeTo(null);  // to center it
	        interfG.setVisible(true);
	        
	    }
	 
    public static void main( String[] args )
    {
    	
		interfGraf();
     
    }
    
   
}
