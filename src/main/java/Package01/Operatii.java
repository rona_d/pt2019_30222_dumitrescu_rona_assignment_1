package Package01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class Operatii {

	/**
	 * Metoda face deep cloning a unei liste de monoame
	 * @param listMonom este o  lista de monoame 
	 * @return lista copiata
	 */
	protected static List<Monom> deepClone(List<Monom> listMonom) {

		ArrayList<Monom> monomListClone = new ArrayList<Monom>();
		Iterator<Monom> iterator = listMonom.iterator();

		while (iterator.hasNext()) {
			// Add the object clones
			
			Monom monomDurere = new Monom();
			Monom monomCopy = iterator.next();
		
			monomDurere.setCoef(monomCopy.getCoef());
			monomDurere.setGrad(monomCopy.getGrad());

			monomListClone.add(monomDurere);

		}

		return monomListClone;
	}

	/**
	 * Metoda aduna doua polinoame pe care le primeste ca parametru
	 * @param p1 - polinomul 1
	 * @param p2 - polinomul 2
	 * @return rezultatul adunarii celor 2 polinoame
	 */
 	protected static Polinom adunare(Polinom p1, Polinom p2) {

		Polinom rez = new Polinom();
		rez.setListaPolinom(deepClone(p1.getListaPolinom())); // copiem polinomul 2 in polinomul rez

		for (Monom m2 : p2.getListaPolinom()) {
			
			boolean gradAdded = false;
			for (Monom m1 : rez.getListaPolinom()) {
				
				if (m1.getGrad().equals(m2.getGrad())) {
					
					m1.setCoef(m1.getCoef() + m2.getCoef());
					
					gradAdded = true;
				}
			}
			if (!gradAdded) {
				rez.getListaPolinom().add(m2);
			}
		}
		
		Collections.reverse(rez.getListaPolinom());
		return rez;
	}

	protected static Polinom scadere(Polinom p1, Polinom p2) {
		
		Polinom rez = new Polinom();
		rez.setListaPolinom(deepClone(p1.getListaPolinom()));
		
		for (Monom m2 : p2.getListaPolinom()) {
			boolean gradAdded = false;

			for (Monom m1 : rez.getListaPolinom()) {
				if (m2.getGrad() == m1.getGrad()) {
					m1.setCoef(m1.getCoef() - m2.getCoef());
					gradAdded = true;
				}
			}

			if (!gradAdded) {
				Monom m2Copie = new Monom();
				m2Copie.setGrad(m2.getGrad());
				m2Copie.setCoef(0 - m2.getCoef());
				rez.getListaPolinom().add(m2Copie);
			}
		}

		return rez;
	}

	protected static Polinom inmultire(Polinom p1, Polinom p2) {
		Polinom rez = new Polinom();

		List<Monom> listaP = new ArrayList<Monom>();

		for (Monom m1 : p1.getListaPolinom()) {
			for (Monom m2 : p2.getListaPolinom()) {
				Monom auxMonom = new Monom();

				auxMonom.setGrad(m1.getGrad() + m2.getGrad());
				auxMonom.setCoef(m1.getCoef() * m2.getCoef());

				if (listaP.isEmpty() == true) {
					listaP.add(auxMonom);
				} else {
					for (Monom monomVerif : listaP) {
						if (monomVerif.getGrad().equals(auxMonom.getGrad())) {
							monomVerif.setCoef(monomVerif.getCoef() + auxMonom.getCoef());
						}
					}
					listaP.add(auxMonom);
				}
			}
		}

		rez.setListaPolinom(listaP);
		return rez;
	}

	protected static Polinom derivare(Polinom p) {
		Polinom rez = new Polinom();

		for (Monom monomParcurgePolinom : p.getListaPolinom()) {

			Monom m = new Monom();
			m.setGrad(monomParcurgePolinom.getGrad() - 1);
			m.setCoef(monomParcurgePolinom.getCoef() * monomParcurgePolinom.getGrad());

			rez.listaPolinom.add(m);
		}

		return rez;
	}

	protected static Polinom integrare(Polinom p) {
		Polinom rez = new Polinom();

		for (Monom mParcurgere : p.getListaPolinom()) {
			Monom mCopie = new Monom();
			mCopie.setGrad(mParcurgere.getGrad() + 1);
			mCopie.setCoef(mParcurgere.getCoef() / (mParcurgere.getGrad() + 1));
			rez.listaPolinom.add(mCopie);
		}

		return rez;
	}

}
