package Package01;

import junit.framework.TestCase;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;


//import static junit.framework.TestCase.assertEquals;


/**
 * @date 21.03.2019
 * @author Rona Dumitrescu
 *
 */

public class MainTest extends TestCase {
	
	private Polinom p1;
	private Polinom p2;

	@Before
	public void init()
	{
		p1 = new Polinom();
		p2 = new Polinom();
		
		Monom monom1 = new Monom(Float.valueOf(5),Integer.valueOf(4));
		Monom monom2 = new Monom(Float.valueOf(3),Integer.valueOf(3));
		Monom monom3 = new Monom(Float.valueOf(2),Integer.valueOf(3));
		Monom monom4 = new Monom(Float.valueOf(-2),Integer.valueOf(2));
		
		p1.addMonom(monom1);
		p1.addMonom(monom2);
		
		p2.addMonom(monom3);
		p2.addMonom(monom4);
			
	}

	// ADUNARE
	@Test
	public void testAdunare()
	{
		init();
		Polinom expectedPolinom = new Polinom();
		
		Monom monom1 = new Monom(Float.valueOf(5),Integer.valueOf(4));
		Monom monom2 = new Monom(Float.valueOf(5),Integer.valueOf(3));
		Monom monom3 = new Monom(Float.valueOf(-2),Integer.valueOf(2));
		
		expectedPolinom.addMonom(monom1);
		expectedPolinom.addMonom(monom2);
		expectedPolinom.addMonom(monom3);
		
		Polinom actualPolinom = new Polinom();
		actualPolinom = Operatii.adunare(p1, p2);  
	
		Collections.reverse(actualPolinom.getListaPolinom());
	
		assertEquals(expectedPolinom.getListaPolinom().get(0).getCoef(), actualPolinom.getListaPolinom().get(0).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(0).getGrad(), actualPolinom.getListaPolinom().get(0).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(1).getCoef(), actualPolinom.getListaPolinom().get(1).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(1).getGrad(), actualPolinom.getListaPolinom().get(1).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(2).getCoef(), actualPolinom.getListaPolinom().get(2).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(2).getGrad(), actualPolinom.getListaPolinom().get(2).getGrad());

	}
	
	//SCADERE
	@Test
	public void testScadere()
	{
		
		init();
		Polinom expectedPolinom = new Polinom();
		
		Monom monom1 = new Monom(Float.valueOf(5),Integer.valueOf(4));
		Monom monom2 = new Monom(Float.valueOf(1),Integer.valueOf(3));
		Monom monom3 = new Monom(Float.valueOf(2),Integer.valueOf(2));
		
		expectedPolinom.addMonom(monom1);
		expectedPolinom.addMonom(monom2);
		expectedPolinom.addMonom(monom3);
		
		Polinom actualPolinom = new Polinom();
		actualPolinom = Operatii.scadere(p1, p2);  
	
		Collections.reverse(expectedPolinom.getListaPolinom());
		Collections.reverse(actualPolinom.getListaPolinom());
	
		assertEquals(expectedPolinom.getListaPolinom().get(0).getCoef(), actualPolinom.getListaPolinom().get(0).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(0).getGrad(), actualPolinom.getListaPolinom().get(0).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(1).getCoef(), actualPolinom.getListaPolinom().get(1).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(1).getGrad(), actualPolinom.getListaPolinom().get(1).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(2).getCoef(), actualPolinom.getListaPolinom().get(2).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(2).getGrad(), actualPolinom.getListaPolinom().get(2).getGrad());

	}
	
	//INMULTIRE
	/*@Test
	public void testInmultire()
	{
		init();
		Polinom expectedPolinom = new Polinom();
		
		Monom monom1 = new Monom(Float.valueOf(10),Integer.valueOf(7));
		Monom monom2 = new Monom(Float.valueOf(-4),Integer.valueOf(6));
		Monom monom3 = new Monom(Float.valueOf(-6),Integer.valueOf(5));
		
		expectedPolinom.addMonom(monom1);
		expectedPolinom.addMonom(monom2);
		expectedPolinom.addMonom(monom3);
		
		Polinom actualPolinom = new Polinom();
		actualPolinom = Operatii.inmultire(p1, p2);  
	
		Collections.reverse(expectedPolinom.getListaPolinom());
		Collections.reverse(actualPolinom.getListaPolinom());
	
		assertEquals(expectedPolinom.getListaPolinom().get(0).getCoef(), actualPolinom.getListaPolinom().get(0).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(0).getGrad(), actualPolinom.getListaPolinom().get(0).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(1).getCoef(), actualPolinom.getListaPolinom().get(1).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(1).getGrad(), actualPolinom.getListaPolinom().get(1).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(2).getCoef(), actualPolinom.getListaPolinom().get(2).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(2).getGrad(), actualPolinom.getListaPolinom().get(2).getGrad());

		
	}
	*/
	
	// DERIVARE
	@Test
	public void testDerivare()
	{
		init();
		Polinom expectedPolinom = new Polinom();
		
		Monom monom1 = new Monom(Float.valueOf(20),Integer.valueOf(3));
		Monom monom2 = new Monom(Float.valueOf(9),Integer.valueOf(2));
				
		expectedPolinom.addMonom(monom1);
		expectedPolinom.addMonom(monom2);
				
		Polinom actualPolinom = new Polinom();
		actualPolinom = Operatii.derivare(p1);  
	
		Collections.reverse(expectedPolinom.getListaPolinom());
		Collections.reverse(actualPolinom.getListaPolinom());
	
		assertEquals(expectedPolinom.getListaPolinom().get(0).getCoef(), actualPolinom.getListaPolinom().get(0).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(0).getGrad(), actualPolinom.getListaPolinom().get(0).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(1).getCoef(), actualPolinom.getListaPolinom().get(1).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(1).getGrad(), actualPolinom.getListaPolinom().get(1).getGrad());
	
	}
	
	@Test
	public void testIntegrare()
	{
		init();
		Polinom expectedPolinom = new Polinom();
		
		
		Monom monom1 = new Monom(Float.valueOf(1),Integer.valueOf(5));
		Monom monom2 = new Monom(Float.valueOf(3/4f),Integer.valueOf(4));
				
		expectedPolinom.addMonom(monom1);
		expectedPolinom.addMonom(monom2);
				
		Polinom actualPolinom = new Polinom();
		actualPolinom = Operatii.integrare(p1);  
	
		Collections.reverse(expectedPolinom.getListaPolinom());
		Collections.reverse(actualPolinom.getListaPolinom());
	
		assertEquals(expectedPolinom.getListaPolinom().get(0).getCoef(), actualPolinom.getListaPolinom().get(0).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(0).getGrad(), actualPolinom.getListaPolinom().get(0).getGrad());
		
		assertEquals(expectedPolinom.getListaPolinom().get(1).getCoef(), actualPolinom.getListaPolinom().get(1).getCoef());
		assertEquals(expectedPolinom.getListaPolinom().get(1).getGrad(), actualPolinom.getListaPolinom().get(1).getGrad());		
		
	}


}
